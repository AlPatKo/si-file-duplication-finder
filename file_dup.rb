#!/usr/bin/env ruby
require 'digest'
require 'pp'

def get_all_filenames(dir)
	Dir.chdir dir
	dirs = Dir.glob("**/*")
	files = []
	dirs.each do |e|
		files << e if File.file?(e) && File.size(e) > 0
	end
	files
end

def hash_files(filenames)
	file_hashes = {}
	filenames.each do |f| 
		h = Digest::SHA512.hexdigest File.read f
		file_hashes.merge!(File.absolute_path(f) => h)
	end
	file_hashes
end

def compare(source, target)
	duplicates = []
	source.each do |s_key, s_val|
		target.each do |t_key, t_val|
			duplicates << [s_key, t_key] if s_val.eql?(t_val) && !s_key.eql?(t_key)
		end
	end
	duplicates.each{|sub| sub.sort!}
	duplicates.uniq
end

def main
	sdir = ARGV[0].to_s.chomp
	tdir = ARGV[1].to_s.chomp
	if Dir.exist?(sdir) && Dir.exist?(tdir) 
		pp compare(hash_files(get_all_filenames(sdir)), hash_files(get_all_filenames(tdir)))

	else
		pp "Invalid director(y/ies)"
	end
end

main