##InfoSec Lab #1

###About hashing functions
Hash functions are functions that are used to map data of various sizes to chunks of data of a fixed size, depending on algorithm. This way data lookup can be done in a easier way, i.e. instead of checking entire file/piece of data, user only has to compare checksums in order to find duplicates of data. A hash function digests(takes as a parameter) an arbitrary piece of data of various size, i.e. a string/file/movie/song, anything that consists of bits and spits out a hash (aka hash code, aka hash value, aka checksum) of fixed size. Most commonly used hash functions nowadays are MD5(not secure anymore), SHA1(common), SHA2(growing strong), SHA3(just in case).

The uses of hashing functions:

+ Fast data lookup using hash tables
+ Duplicate check
+ Security (storing hashes of passwords instead of passwords themselves)
+ Creating caches of huge data
+ Finding similarities in data
+ Protecting data
+ Verification of authenticity of data(check if it was not intercepted and changed on the way)

Hashing functions' properties:

+ They're irreversible, meaning: having a hash, we cannot compute the original data
+ Infinite number of inputs (anything can be digested)
+ Finite set of outputs
+ No two exact data chunks can produce the same output(theoretical, since the number of outputs is limited)
+ A small change in input leads to big change in the output
+ Two exact data chunks will produce the same output, regardless of the machine/architecture/language they run on, if they use the same hashing function

Due to the 2nd and 3rd properties, sometimes functions may yield collisions, i.e. two different inputs give the same output. this happens due to the fact that we live in real world, and everything is quite finite, including number of bits used in the algorithm of given function. Thus, it can only produce so many outputs, whereas number of inputs it can digest is infinite. However, with technology advancing, new algorithms appear that are harder, better faster, stronger and use more bits, allowing them to produce more possible outcomes.

###Short description (very)
This lab's purpose is for us to get to know hashing functions.
I chose to create a script that, given two directories, seeks file duplications.
In my script I used ruby module Digest and its SHA512(over engineering like pro), which is an implementation of SHA2 function developed by NSA. Why? Well, my reasoning: "There are lots of files, some of them are super-duper long, maybe that can cause a collision". That is the short version why I used SHA512. This script can be run in shell and takes two arguments as directories.

###About the app
This application is written in ruby, but it can be run in terminal as a script taking two string arguments, that should be valid directories. If at least one of them is missing/invalid, the script will return a message "Invalid director(y/ies)". In case of valid args, the app will show and array of arrays, each subarray containing pairs of duplicate files. I made it so the app doesn't check empty files, and also doesn't return permuted pairs. 

###Conclusion
The application works, or at least pretends(quite nice) to be working. Though, using SHA512 for the purpose seems a bit(a tad more than that) of an overkill, it is still faster than SHA256 and a tad slower than SHA1, so I decided(to hell with performance) let it be more collisionwise secure. It was an interesting task to execute, not too hard and not too easy, just the right amount of challenge to keep one going. The biggest milestone I have encountered was refactoring file detection, so it would only render the adequate outputs, i.e. deciding what is the relevant info for output.